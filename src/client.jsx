    import React from 'react';
    import {hydrate} from 'react-dom';
    import {BrowserRouter} from 'react-router-dom';
    import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
    import {lightBlue, red} from '@material-ui/core/colors';
    import Index from './app/index';
    import RTL from './app/public/rtl';

    const theme = createMuiTheme({
        palette: {
            primary: lightBlue,
            accent: red,
            type: 'light',
        },
        direction: 'rtl',
    });

    class Main extends React.Component {
        // Remove the server-side injected CSS.
        componentDidMount() {
            const jssStyles = document.getElementById('jss-server-side');
            if (jssStyles && jssStyles.parentNode) {
                jssStyles.parentNode.removeChild(jssStyles);
            }
        }

        render() {
            return (
                <BrowserRouter>
                    <Index {...this.props}/>
                </BrowserRouter>
            );
        }
    }

    hydrate((
        <RTL>
            <MuiThemeProvider theme={theme}>
                <Main/>
            </MuiThemeProvider>
        </RTL>
    ), document.getElementById('root'));
