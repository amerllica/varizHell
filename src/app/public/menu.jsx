import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {NavLink} from 'react-router-dom';
import styles from 'StylesRoot/styles.pcss';
import jssStyles from 'jssRoot/jssStyles.jss';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class Menu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            usersList: false,
            transitionsList: false,
            accountsList: false,
            ticketsList: false,
        };
    };

    componentDidMount() {

        if (typeof window !== 'undefined') {

            this.expandedBasedOnLocationHandler(location.pathname.substring(1));
        }
    };

    expandedBasedOnLocationHandler = (location) => {
        switch (location) {
            case 'users-list':
                this.setState({
                    usersList: true,
                });
                break;
            case 'transitions-list':
                this.setState({
                    transitionsList: true,
                });
                break;
            case 'accounts-list':
                this.setState({
                    accountsList: true,
                });
                break;
            case 'tickets-list':
                this.setState({
                    ticketsList: true,
                });
                break;
            default:
                return;
        }
    };

    expandHandler = (state) => {
        this.setState({
            [state]: !this.state[state],
        })
    };

    render() {
        const {classes} = this.props;

        return (
            <nav className={styles.menu}>
                <ExpansionPanel expanded={this.state.usersList}>
                    <ExpansionPanelSummary
                        classes={{root: classes.panelSummary, expanded: classes.panelSummaryExpanded}}
                        onClick={(e) => e.stopPropagation()}
                    >
                        <Button classes={{root: classes.buttonInExpansion}}
                                onClick={() => this.expandHandler('usersList')}>
                            مدیریت کاربران
                        </Button>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails classes={{root: classes.expansionPanelDetails}}>
                        <NavLink exact activeClassName={styles.active} to={'/users-list'}>لیست کاربران</NavLink>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={this.state.transitionsList}>
                    <ExpansionPanelSummary
                        classes={{root: classes.panelSummary, expanded: classes.panelSummaryExpanded}}
                        onClick={(e) => e.stopPropagation()}
                    >
                        <Button classes={{root: classes.buttonInExpansion}}
                                onClick={() => this.expandHandler('transitionsList')}>
                            مدیریت تراکنش‌ها
                        </Button>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails classes={{root: classes.expansionPanelDetails}}>
                        <NavLink exact activeClassName={styles.active} to={'/transitions-list'}>لیست تراکنش‌ها</NavLink>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={this.state.accountsList}>
                    <ExpansionPanelSummary
                        classes={{root: classes.panelSummary, expanded: classes.panelSummaryExpanded}}
                        onClick={(e) => e.stopPropagation()}
                    >
                        <Button classes={{root: classes.buttonInExpansion}}
                                onClick={() => this.expandHandler('accountsList')}>
                            مدیریت حساب‌ها
                        </Button>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails classes={{root: classes.expansionPanelDetails}}>
                        <NavLink exact activeClassName={styles.active} to={'/accounts-list'}>لیست شماره
                            حساب‌ها</NavLink>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={this.state.ticketsList}>
                    <ExpansionPanelSummary
                        classes={{root: classes.panelSummary, expanded: classes.panelSummaryExpanded}}
                        onClick={(e) => e.stopPropagation()}
                    >
                        <Button classes={{root: classes.buttonInExpansion}}
                                onClick={() => this.expandHandler('ticketsList')}>
                            مدیریت تیکت‌ها
                        </Button>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails classes={{root: classes.expansionPanelDetails}}>
                        <NavLink exact activeClassName={styles.active} to={'/tickets-list'}>لیست تیکت‌ها</NavLink>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </nav>
        );
    };
}

Menu.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(Menu);
