    import React, {Component} from 'react';
    import Helmet from 'react-helmet';
    import styles from 'StylesRoot/styles.pcss';
    import TextField from '@material-ui/core/TextField';

    export default class App extends Component {

        render() {
            return (
                <div className={styles['root-wrapper']}>
                    <Helmet
                        htmlAttributes={{lang: 'fa', amp: undefined}}
                        bodyAttributes={{dir: 'rtl'}}
                        titleTemplate='اسکن - %s'
                        titleAttributes={{itemprop: 'name', lang: 'fa'}}
                        meta={[
                            {name: 'description', content: 'صفحه اتصال اعضاء'},
                            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
                        ]}
                        link={[{rel: 'stylesheet', href: '/dist/styles.css'}]}
                    />
                    <TextField label='test' helperText='help'/>
                </div>
            );
        };
    }
