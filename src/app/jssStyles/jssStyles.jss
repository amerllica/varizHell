import variables from './partials/variables.jss';

module.exports = {
    /** @define Form Custom Styles */
    formControlSelectWrapper: {
        width: '200px',
    },
    formControl: {
        background: variables.formControlBackground,
        borderRadius: '3px',
        marginBottom: '20px'
    },
    input: {
        /*paddingLeft: '25px',*/
        /*direction: 'ltr',*/
        color: variables.main,
        '&:before': {
            borderColor: `${variables.light} !important`,
            borderRadius: '0 0 3px 3px',
        },
        '&:after': {
            /*borderColor: variables.focused,*/
            borderRadius: '0 0 3px 3px',
        }
    },
    inputLabel: {
        /*left: '25px',*/
        fontSize: '15px',
        color: variables.light,
        top: '-2px',
    },
    inputLabelShrink: {
        top: '5px',
        color: `${variables.main} !important`,
        zIndex: 1,
    },
    inputAdornment: {
        marginLeft: 0,
        transform: 'translateY(-7px)',
    },
    inputAdornmentIconButton: {
        color: variables.light
    },
    submitButton: {
        marginTop: '55px',
        /*background: '#000',
         color: variables.focused,
         '&:hover': {
         background: 'rgba(0,0,0,.75)'
         }*/
    },

    /** @define App bar Custom Style */

    appBar: {
        position: 'absolute',
    },

    /** @define Expansion Panels Custom Style */
    panelSummary: {
        minHeight: 'auto',
        padding: 0,

        '& > div': {
            margin: 0,
        },
    },
    panelSummaryExpanded: {
        minHeight: 'auto !important',

        '& > div': {
            margin: '0 !important',
        }
    },
    expansionPanelDetails: {
        flexDirection: 'column',
        fontSize: '15px',
        paddingRight: '40px',
    },
    buttonInExpansion: {
        width: '100%',
        justifyContent: 'flex-start',
    },

    /** @define Paper table wrapper */
    paperTableWrapper:{
        overflowX: 'auto',
        scrollBehavior: 'smooth',
        WebkitOverflowScrolling: 'touch',
        transform: 'translateZ(0)',

        '&::-webkit-scrollbar': {
            display: 'none',
        }
    },

    /** @define Form control label */
    formControllabelText: {
        userSelect: 'none',
    }
};
