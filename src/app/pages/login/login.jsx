import React, {Component} from 'react';
import Helmet from 'react-helmet';
import styles from 'StylesRoot/styles.pcss';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import jssStyles from 'jssRoot/jssStyles.jss';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import HighlightOff from '@material-ui/icons/HighlightOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            showPassword: false,
            renderDOM: false,
        };
    }

    componentDidMount = () => {

        setTimeout(() => {
            if (!JSON.parse(localStorage.getItem('loginDetails'))) {
                this.setState({
                    renderDOM: true,
                })
            }
        }, 0);
    };

    handleMouseDownPassword = e => {
        e.preventDefault();
    };

    handleClickShowPassword = () => this.setState({showPassword: !this.state.showPassword});

    handleClickResetUsername = () => {
        this.setState({
            username: '',
            password: '',
        });
        document.getElementById('adornment-username').focus();
    };

    render() {

        const {
            loginHandler,
            classes
        } = this.props;

        return (
            this.state.renderDOM
                ? (
                    <div className={styles.login}>
                        <Helmet title={`صفحه ورود داشبورد`}/>

                        <div className={styles.login__form}>
                            <div className={styles['input-wrapper']}>
                        <span className={styles.welcome}>
                            داشبورد اسکن
                        </span>
                                <span className={styles['welcome-hint']}>
                            برای ورود به داشبورد نام کاربری و رمز عبور خود را وارد کنید
                        </span>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="name-simple" className={classes.inputLabel}
                                                classes={{shrink: classes.inputLabelShrink}}>نام کاربری</InputLabel>
                                    <Input type='username' value={this.state.username}
                                           id='adornment-username'
                                           classes={{root: classes.input}}
                                           onChange={(e) => this.setState({
                                               username: e.target.value,
                                           })}
                                           onKeyPress={(e) => {
                                               if (e.key === 'Enter') {
                                                   document.getElementById('adornment-password').focus();
                                               }
                                           }}
                                           endAdornment={
                                               <InputAdornment position="end" classes={{root: classes.inputAdornment}}>
                                                   <IconButton
                                                       classes={{root: classes.inputAdornmentIconButton}}
                                                       aria-label="Reset Username"
                                                       onClick={this.handleClickResetUsername}
                                                       onMouseDown={this.handleMouseDownPassword}
                                                   >
                                                       <HighlightOff/>
                                                   </IconButton>
                                               </InputAdornment>
                                           }
                                    />
                                </FormControl>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor='adornment-password' className={classes.inputLabel}
                                                classes={{shrink: classes.inputLabelShrink}}
                                    >
                                        رمز عبور
                                    </InputLabel>
                                    <Input
                                        id='adornment-password'
                                        classes={{root: classes.input}}
                                        type={this.state.showPassword ? 'username' : 'password'}
                                        autoComplete='new-password'
                                        value={this.state.password}
                                        onChange={(e) => this.setState({
                                            password: e.target.value,
                                        })}
                                        onKeyPress={(e) => {
                                            if (e.key === 'Enter') {
                                                document.getElementById('submit').click();
                                            }
                                        }}
                                        endAdornment={
                                            <InputAdornment position="end" classes={{root: classes.inputAdornment}}>
                                                <IconButton
                                                    classes={{root: classes.inputAdornmentIconButton}}
                                                    aria-label="Toggle password visibility"
                                                    onClick={this.handleClickShowPassword}
                                                    onMouseDown={this.handleMouseDownPassword}
                                                >
                                                    {this.state.showPassword ? <VisibilityOff/> : <Visibility/>}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                                <Button variant='contained' id='submit' color="primary"
                                        onClick={() => loginHandler(this.state.username, this.state.password)}
                                        classes={{root: classes.submitButton}}>
                                    ورود
                                </Button>
                            </div>
                        </div>
                    </div>
                )
                : null
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(Login);
