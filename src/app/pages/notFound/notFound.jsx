import React from 'react';
import Helmet from 'react-helmet';
import styles from 'StylesRoot/styles.pcss';

export default () => (
    <section className={styles['not-found']}>
        <Helmet title='404'/>
        <h1>Not Found</h1>
    </section>
);