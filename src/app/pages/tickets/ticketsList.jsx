import React from 'react';
import Helmet from 'react-helmet';
import cloneDeep from 'lodash/cloneDeep';
import config from 'config';
import styles from 'StylesRoot/styles.pcss';
import jssStyles from 'jssRoot/jssStyles.jss';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const website = config.website;

const ticketsDataListUrl = `${website}/api/v1/support/tickets`;

class TicketsList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ticketsData: {},
            wannaLoadTicketsData: true,
            ticketsDataSize: 6,
            ticketsDataPage: 0,
        };

        ({
            fetchManager: this.fetchManager,
        } = props);
    };

    componentDidMount() {

        this.getTicketsData();
    };

    getTicketsData = async (size = this.state.ticketsDataSize, page = this.state.ticketsDataPage) => {

        const result = await this.fetchManager({
            url: `${ticketsDataListUrl}?size=${size}&page=${page}`,
            method: `GET`,
        });

        if (typeof result !== 'undefined' && Object.keys(result).length) {

            this.setState({
                ticketsData: result,
            });
        }

        console.log(this.state.ticketsData);
    };

    renderTicketsList = () => {

        const {classes} = this.props;

        const {
            content = [],
        } = cloneDeep(this.state.ticketsData);

        return (
            content.length
                ? (
                    <Paper classes={{root: classes.paperTableWrapper}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>کاربری</TableCell>
                                    <TableCell>تایپ</TableCell>
                                    <TableCell>پولکیف</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    content.map((ticket, index) => (
                                        <TableRow key={ticket.id} className={index === 0 ? styles['first-row'] : ''}>
                                            <TableCell>
                                                {ticket.id}
                                            </TableCell>
                                            <TableCell>
                                                {ticket.type}
                                            </TableCell>
                                            <TableCell>
                                                {ticket.walletBalance}
                                            </TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                            {/*<TableFooter>
                            <TableRow>
                                <TableCell colSpan={4}>
                                    <TablePagination
                                        component={props => props.children}
                                        count={this.state.paymentsList.totalPages} page={this.state.paymentsListPage}
                                        rowsPerPage={this.state.paymentsListSize} rowsPerPageOptions={[1, 2, 3, 4, 5, 6]}
                                        labelRowsPerPage={`تعداد سطر در هر صفحه`}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActionsWrapped}
                                        labelDisplayedRows={({from, to, count}) => `${persianJs(from).englishNumber()}-${persianJs(to).englishNumber()} از ${persianJs(count).englishNumber()}`}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableFooter>*/}
                        </Table>
                    </Paper>
                )
                : null
        );
    };

    render() {

        return (
            <Grid container className={`${styles['tickets-list']} ${styles['page-wrapper']}`}>
                <Helmet title='لیست تیکت‌ها'/>
                {this.renderTicketsList()}
            </Grid>
        )
    }
}

TicketsList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(TicketsList);
