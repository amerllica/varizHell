import React from 'react';
import Helmet from 'react-helmet';
import cloneDeep from 'lodash/cloneDeep';
import config from 'config';
import styles from 'StylesRoot/styles.pcss';
import jssStyles from 'jssRoot/jssStyles.jss';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import FilterList from '@material-ui/icons/FilterList';
import ClearAll from '@material-ui/icons/ClearAll';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const website = config.website;

const usersDataListUrl = `${website}/api/v1/support/users`;

class UsersList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            usersData: {},
            wannaLoadUsersData: true,
            usersDataSize: 6,
            usersDataPage: 0,
            modalOpen: false,
            revokeUserId: -1,
            revokeQuestion: false,
            filterUnit: false,
            isFilterSettled: false,
            filterField_id: '', /* equal */
            filterField_username: '', /* like */
            filterField_firstName: '', /* like */
            filterField_lastName: '', /* like */
            filterField_email: '', /* like */
            filterField_nationalNumber: '', /* equal */
            filterField_enabled: '', /* equal */

        };

        ({
            fetchManager: this.fetchManager,
        } = props);
    };

    componentDidMount() {

        this.getUsersData();
    };

    handleModalClose = () => {

        this.setState({
            modalOpen: false,
            revokeQuestion: false,
            filterUnit: false,
        });

        this.handleFilterFieldReset();
    };

    handleFilterFieldChange = stateName => event => this.setState({
        [stateName]: event.target.value,
    });

    handleIsFilterSettled = (isFilterSettled) => this.setState({
        isFilterSettled,
    });

    handleFilterFieldReset = () => this.setState({
        filterField_id: '',
        filterField_username: '',
        filterField_firstName: '',
        filterField_lastName: '',
        filterField_email: '',
        filterField_nationalNumber: '',
        filterField_enabled: '',
    });

    revokeUser = async (userId = this.state.revokeUserId) => {


        const result = await this.fetchManager({
            url: `${usersDataListUrl}/${userId}`,
            method: `DELETE`,
        });


        if (result.ok) {

            this.getUsersData();

        } else {
            console.log(await result.json());
            throw new Error(`revoke user of usersList has error`);
        }
    };

    getUsersData = async (size = this.state.usersDataSize, page = this.state.usersDataPage) => {

        const
            filterField_id = this.state.filterField_id !== ''
                ? `&id=${this.state.filterField_id}`
                : '',

            filterField_username = this.state.filterField_username !== ''
                ? `&username=${this.state.filterField_username}`
                : '',

            filterField_firstName = this.state.filterField_firstName !== ''
                ? `&firstName=${this.state.filterField_firstName}`
                : '',

            filterField_lastName = this.state.filterField_lastName !== ''
                ? `&lastName=${this.state.filterField_lastName}`
                : '',

            filterField_email = this.state.filterField_email !== ''
                ? `&email=${this.state.filterField_email}`
                : '',

            filterField_nationalNumber = this.state.filterField_nationalNumber !== ''
                ? `&nationalNumber=${this.state.filterField_nationalNumber}`
                : '',

            filterField_enabled = this.state.filterField_enabled !== ''
                ? `&enabled=${this.state.filterField_enabled}`
                : '';

        const result = await this.fetchManager({
            url: `${usersDataListUrl}?size=${size}&page=${page}${filterField_id}${filterField_username}${filterField_firstName}${filterField_lastName}${filterField_email}${filterField_nationalNumber}${filterField_enabled}`,
            method: `GET`,
        });

        if (typeof result !== 'undefined' && Object.keys(result).length) {

            this.setState({
                usersData: result,
            })
        }

        console.log(this.state.usersData);
    };

    renderRevokeQuestion = () => (
        <Paper className={styles['modal-paper-content']}>
            <Grid className={styles['modal-paper-content__wrapper']}>
                <Grid className={styles['modal-paper-content__wrapper--title']}>آیا برای الغا کردن این کاربر
                    اطمینان کامل دارید؟
                </Grid>
                <Grid className={styles['modal-paper-content__wrapper--buttons']}>
                    <Button size='small' color="secondary"
                            onClick={() => {
                                this.handleModalClose();
                                this.revokeUser();
                            }}>
                        بلی
                    </Button>
                    <Button size='large' variant='contained' color="primary"
                            action={it => it.focusVisible()}
                            onClick={this.handleModalClose}>خیر</Button>
                </Grid>
            </Grid>
        </Paper>
    );

    renderFilterUnit = () => {

        const {
            classes,
        } = this.props;

        return (
            <Paper className={styles['modal-paper-content']}>
                <Grid container className={styles['modal-paper-content__wrapper']}>
                    <Grid container className={styles['users-filter']}>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <TextField type='number' name='id' label='آیدی'
                                       value={this.state.filterField_id}
                                       onChange={this.handleFilterFieldChange('filterField_id')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <TextField type='text' name='username' label='نام کاربری'
                                       value={this.state.filterField_username}
                                       onChange={this.handleFilterFieldChange('filterField_username')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <TextField type='text' name='firstName' label='نام'
                                       value={this.state.filterField_firstName}
                                       onChange={this.handleFilterFieldChange('filterField_firstName')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <TextField type='text' name='lastName' label='نام خانوادگی'
                                       value={this.state.filterField_lastName}
                                       onChange={this.handleFilterFieldChange('filterField_lastName')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <TextField type='email' name='email' label='ایمیل'
                                       value={this.state.filterField_email}
                                       onChange={this.handleFilterFieldChange('filterField_email')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <TextField type='number' name='nationalNumber' label='شماره ملی'
                                       value={this.state.filterField_nationalNumber}
                                       onChange={this.handleFilterFieldChange('filterField_nationalNumber')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}>
                            <FormControlLabel
                                classes={{label: classes.formControllabelText}}
                                control={
                                    <Switch checked={this.state.filterField_enabled}
                                            onChange={() => this.setState({
                                                filterField_enabled: !this.state.filterField_enabled,
                                            })}/>
                                }
                                label='اجازه فعالیت'
                            />

                        </Grid>
                        <Grid item xs={12} md={6} className={styles['users-filter__grid']}/>
                        <Grid item xs={12} className={styles['users-filter__submit-wrapper']}>
                            <Button variant='contained' color='primary' onClick={() => {
                                this.handleIsFilterSettled(true);
                                this.getUsersData();
                                this.handleModalClose();
                            }}>
                                اعمال
                            </Button>
                        </Grid>


                    </Grid>
                </Grid>
            </Paper>
        );
    };

    renderModalContent = () => {

        if (this.state.revokeQuestion) {

            return this.renderRevokeQuestion();

        } else if (this.state.filterUnit) {

            return this.renderFilterUnit();

        } else {

            return null;
        }
    };

    renderUsersList = () => {

        const {classes} = this.props;

        const {
            content = [],
        } = cloneDeep(this.state.usersData);

        const loggedInUserId = atob(JSON.parse(localStorage.getItem('loginDetails')).ud);

        return (
            <Paper classes={{root: classes.paperTableWrapper}}>
                <Grid container justify='space-between'>
                    <IconButton onClick={() => this.setState({
                        modalOpen: true,
                        filterUnit: true,
                    })}>
                        <FilterList/>
                    </IconButton>
                    {
                        this.state.isFilterSettled
                            ? (
                                <IconButton color='secondary' onClick={() => {
                                    this.handleIsFilterSettled(false);
                                    this.handleFilterFieldReset();
                                    this.getUsersData();
                                }}>
                                    <ClearAll/>
                                </IconButton>
                            )
                            : null
                    }
                </Grid>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>کاربری</TableCell>
                            <TableCell>تایپ</TableCell>
                            <TableCell>موجودی کیف</TableCell>
                            <TableCell>عملیات</TableCell>
                        </TableRow>
                    </TableHead>
                    {
                        content.length
                            ? (
                                <TableBody>
                                    {
                                        content.map((user, index) => (
                                            loggedInUserId !== user.id
                                                ? (
                                                    <TableRow key={user.id} className={index === 0 ? styles['first-row'] : ''}
                                                              disabled={!user.enabled}>
                                                        <TableCell>
                                                <span className={styles['phone-number']}>
                                                    {user.username}
                                                </span>
                                                        </TableCell>
                                                        <TableCell>
                                                            {user.type}
                                                        </TableCell>
                                                        <TableCell>
                                                            {user.walletBalance}
                                                        </TableCell>
                                                        <TableCell>
                                                            <Button color='secondary'
                                                                    onClick={() => this.setState({
                                                                        revokeUserId: user.id,
                                                                        modalOpen: true,
                                                                        revokeQuestion: true,
                                                                    })}>
                                                                الغا
                                                            </Button>
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                                : null
                                        ))
                                    }
                                </TableBody>
                            )
                            : null
                    }
                    {/*<TableFooter>
                            <TableRow>
                                <TableCell colSpan={4}>
                                    <TablePagination
                                        component={props => props.children}
                                        count={this.state.paymentsList.totalPages} page={this.state.paymentsListPage}
                                        rowsPerPage={this.state.paymentsListSize} rowsPerPageOptions={[1, 2, 3, 4, 5, 6]}
                                        labelRowsPerPage={`تعداد سطر در هر صفحه`}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActionsWrapped}
                                        labelDisplayedRows={({from, to, count}) => `${persianJs(from).englishNumber()}-${persianJs(to).englishNumber()} از ${persianJs(count).englishNumber()}`}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableFooter>*/}
                </Table>
            </Paper>
        );
    };

    render() {

        return (
            <Grid container className={`${styles['users-list']} ${styles['page-wrapper']}`}>
                <Helmet title='لیست کاربران'/>
                <Modal open={this.state.modalOpen} onClose={this.handleModalClose}>
                    {this.renderModalContent()}
                </Modal>
                {this.renderUsersList()}
            </Grid>
        )
    }
}

UsersList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(UsersList);
