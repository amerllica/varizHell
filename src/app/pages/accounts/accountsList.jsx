import React from 'react';
import Helmet from 'react-helmet';
import cloneDeep from 'lodash/cloneDeep';
import config from 'config';
import styles from 'StylesRoot/styles.pcss';
import jssStyles from 'jssRoot/jssStyles.jss';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const website = config.website;

const accountsDataListUrl = `${website}/api/v1/support/settlement/bank-accounts`;

class AccountsList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            accountsData: {},
            wannaLoadAccountsData: true,
            accountsDataSize: 6,
            accountsDataPage: 0,
        };

        ({
            fetchManager: this.fetchManager,
        } = props);
    };

    componentDidMount() {

        this.getAccountsData();
    };

    getAccountsData = async (size = this.state.accountsDataSize, page = this.state.accountsDataPage) => {

        const result = await this.fetchManager({
            url: `${accountsDataListUrl}?size=${size}&page=${page}`,
            method: `GET`,
        });

        if (typeof result !== 'undefined' && Object.keys(result).length) {

            this.setState({
                accountsData: result,
            });
        }

        console.log(this.state.accountsData);
    };

    renderAccountsList = () => {

        const {classes} = this.props;

        const {
            content = [],
        } = cloneDeep(this.state.accountsData);

        return (
            content.length
                ? (
                    <Paper classes={{root: classes.paperTableWrapper}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>کاربری</TableCell>
                                    <TableCell>تایپ</TableCell>
                                    <TableCell>پولکیف</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    content.map((account, index) => (
                                        <TableRow key={account.id} className={index === 0 ? styles['first-row'] : ''}>
                                            <TableCell>
                                                {account.id}
                                            </TableCell>
                                            <TableCell>
                                                {account.type}
                                            </TableCell>
                                            <TableCell>
                                                {account.walletBalance}
                                            </TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                            {/*<TableFooter>
                            <TableRow>
                                <TableCell colSpan={4}>
                                    <TablePagination
                                        component={props => props.children}
                                        count={this.state.paymentsList.totalPages} page={this.state.paymentsListPage}
                                        rowsPerPage={this.state.paymentsListSize} rowsPerPageOptions={[1, 2, 3, 4, 5, 6]}
                                        labelRowsPerPage={`تعداد سطر در هر صفحه`}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActionsWrapped}
                                        labelDisplayedRows={({from, to, count}) => `${persianJs(from).englishNumber()}-${persianJs(to).englishNumber()} از ${persianJs(count).englishNumber()}`}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableFooter>*/}
                        </Table>
                    </Paper>
                )
                : null
        );
    };

    render() {

        return (
            <Grid container className={`${styles['accounts-list']} ${styles['page-wrapper']}`}>
                <Helmet title='لیست حساب‌ها'/>
                {this.renderAccountsList()}
            </Grid>
        )
    }
}

AccountsList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(AccountsList);
