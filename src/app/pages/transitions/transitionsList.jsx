import React from 'react';
import Helmet from 'react-helmet';
import cloneDeep from 'lodash/cloneDeep';
import config from 'config';
import styles from 'StylesRoot/styles.pcss';
import jssStyles from 'jssRoot/jssStyles.jss';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import FilterList from '@material-ui/icons/FilterList';
import ClearAll from '@material-ui/icons/ClearAll';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const website = config.website;

const transitionsDataListUrl = `${website}/api/v1/support/transitions`;

class TransitionsList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            transitionsData: {},
            wannaLoadTransitionsData: true,
            transitionsDataSize: 6,
            transitionsDataPage: 0,
            modalOpen: false,
            filterUnit: false,
            isFilterSettled: false,
            filterField_id: '', /* equal */
            filterField_amount: '', /* equal */
            filterField_traceNumber: '', /* equal */
            filterField_type: '', /* equal */
            filterField_username: '', /* equal */
        };

        ({
            fetchManager: this.fetchManager,
        } = props);
    };

    componentDidMount() {

        this.getTransitionsData();
    };

    handleModalClose = () => {

        this.setState({
            modalOpen: false,
            filterUnit: false,

        });

        this.handleFilterFieldReset();
    };

    handleFilterFieldChange = stateName => event => this.setState({
        [stateName]: event.target.value,
    });

    handleIsFilterSettled = (isFilterSettled) => this.setState({
        isFilterSettled,
    });

    handleFilterFieldReset = () => this.setState({
        filterField_id: '',
        filterField_amount: '',
        filterField_traceNumber: '',
        filterField_type: '',
        filterField_username: '',
    });

    getTransitionsData = async (size = this.state.transitionsDataSize, page = this.state.transitionsDataPage) => {

        const
            filterField_id = this.state.filterField_id !== ''
                ? `&id=${this.state.filterField_id}`
                : '',

            filterField_amount = this.state.filterField_amount !== ''
                ? `&amount=${this.state.filterField_amount}`
                : '',

            filterField_traceNumber = this.state.filterField_traceNumber !== ''
                ? `&traceNumber=${this.state.filterField_traceNumber}`
                : '',

            filterField_email = this.state.filterField_type !== ''
                ? `&type=${this.state.filterField_type}`
                : '',

            filterField_username = this.state.filterField_username !== ''
                ? `&username=${this.state.filterField_username}`
                : '';

        const result = await this.fetchManager({
            url: `${transitionsDataListUrl}?size=${size}&page=${page}${filterField_id}${filterField_amount}${filterField_traceNumber}${filterField_email}${filterField_username}`,
            method: `GET`,
        });

        if (typeof result !== 'undefined' && Object.keys(result).length) {

            this.setState({
                transitionsData: result,
            });
        }

        console.log(this.state.transitionsData);
    };

    renderFilterUnit = () => {

        const {
            classes,
        } = this.props;

        return (
            <Paper className={styles['modal-paper-content']}>
                <Grid container className={styles['modal-paper-content__wrapper']}>
                    <Grid container className={styles['transitions-filter']}>
                        <Grid item xs={12} md={6} className={styles['transitions-filter__grid']}>
                            <TextField type='number' name='id' label='آیدی'
                                       value={this.state.filterField_id}
                                       onChange={this.handleFilterFieldChange('filterField_id')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['transitions-filter__grid']}>
                            <TextField type='number' name='amount' label='مبلغ'
                                       value={this.state.filterField_amount}
                                       onChange={this.handleFilterFieldChange('filterField_amount')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['transitions-filter__grid']}>
                            <TextField type='number' name='traceNumber' label='شماره رهگیری'
                                       value={this.state.filterField_traceNumber}
                                       onChange={this.handleFilterFieldChange('filterField_traceNumber')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['transitions-filter__grid']}>
                            <FormControl classes={{root: classes.formControlSelectWrapper}}>
                                <InputLabel htmlFor='filterField_type'>نوع</InputLabel>
                                <Select
                                    value={this.state.filterField_type}
                                    onChange={this.handleFilterFieldChange('filterField_type')}
                                    inputProps={{
                                        id: 'filterField_type',
                                    }}
                                >
                                    <MenuItem value=''>---</MenuItem>
                                    <MenuItem value='PAYMENT'>پرداخت</MenuItem>
                                    <MenuItem value='PAYMENT_REFUND'>بازگشت پرداخت</MenuItem>
                                    <MenuItem value='CHARGE_WALLET'>شارژ کیف</MenuItem>
                                    <MenuItem value='CHARGE_WALLET_REFUND'>بازگشت شارژ کیف</MenuItem>
                                    <MenuItem value='SETTLEMENT'>تسویه</MenuItem>
                                    <MenuItem value='SETTLEMENT_REFUND'>بازگشت تسویه</MenuItem>
                                    <MenuItem value='SWITCH_BALANCE'>انتقال اعتبار</MenuItem>
                                    <MenuItem value='GIFT'>هدیه</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['transitions-filter__grid']}>
                            <TextField type='text' name='username' label='نام کاربری'
                                       value={this.state.filterField_username}
                                       onChange={this.handleFilterFieldChange('filterField_username')}/>
                        </Grid>
                        <Grid item xs={12} md={6} className={styles['transitions-filter__grid']}/>
                        <Grid item xs={12} className={styles['transitions-filter__submit-wrapper']}>
                            <Button variant='contained' color='primary' onClick={() => {
                                this.handleIsFilterSettled(true);
                                this.getTransitionsData();
                                this.handleModalClose();
                            }}>
                                اعمال
                            </Button>
                        </Grid>

                    </Grid>
                </Grid>
            </Paper>
        );
    };

    renderModalContent = () => {

        if (this.state.filterUnit) {

            return this.renderFilterUnit();

        } else {

            return null;
        }
    };

    renderTransitionsList = () => {

        const {classes} = this.props;

        const {
            content = [],
        } = cloneDeep(this.state.transitionsData);

        return (
            <Paper classes={{root: classes.paperTableWrapper}}>
                <Grid container justify='space-between'>
                    <IconButton onClick={() => this.setState({
                        modalOpen: true,
                        filterUnit: true,
                    })}>
                        <FilterList/>
                    </IconButton>
                    {
                        this.state.isFilterSettled
                            ? (
                                <IconButton color='secondary' onClick={() => {
                                    this.handleIsFilterSettled(false);
                                    this.handleFilterFieldReset();
                                    this.getTransitionsData();
                                }}>
                                    <ClearAll/>
                                </IconButton>
                            )
                            : null
                    }
                </Grid>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>کاربری</TableCell>
                            <TableCell>تایپ</TableCell>
                            <TableCell>شماره پیگیری</TableCell>
                        </TableRow>
                    </TableHead>
                    {
                        content.length
                            ? (
                                <TableBody>
                                    {
                                        content.map((transition, index) => (
                                            <TableRow key={transition.id}
                                                      className={index === 0 ? styles['first-row'] : ''}>
                                                <TableCell>
                                                    {transition.id}
                                                </TableCell>
                                                <TableCell>
                                                    {transition.type}
                                                </TableCell>
                                                <TableCell>
                                                    {transition.traceNumber}
                                                </TableCell>
                                            </TableRow>
                                        ))
                                    }
                                </TableBody>
                            )
                            : null
                    }
                    {/*<TableFooter>
                            <TableRow>
                                <TableCell colSpan={4}>
                                    <TablePagination
                                        component={props => props.children}
                                        count={this.state.paymentsList.totalPages} page={this.state.paymentsListPage}
                                        rowsPerPage={this.state.paymentsListSize} rowsPerPageOptions={[1, 2, 3, 4, 5, 6]}
                                        labelRowsPerPage={`تعداد سطر در هر صفحه`}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActionsWrapped}
                                        labelDisplayedRows={({from, to, count}) => `${persianJs(from).englishNumber()}-${persianJs(to).englishNumber()} از ${persianJs(count).englishNumber()}`}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableFooter>*/}
                </Table>
            </Paper>
        );
    };

    render() {

        return (
            <Grid container className={`${styles['transitions-list']} ${styles['page-wrapper']}`}>
                <Helmet title='لیست تراکنش‌ها'/>
                <Modal open={this.state.modalOpen} onClose={this.handleModalClose}>
                    {this.renderModalContent()}
                </Modal>
                {this.renderTransitionsList()}
            </Grid>
        )
    }
}

TransitionsList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(jssStyles)(TransitionsList);
