import React from 'react';
import Helmet from 'react-helmet';
import styles from 'StylesRoot/styles.pcss';

export default (props) => (
    <section className={styles.contact}>
        <Helmet title='تماس باما'/>
        <h1>
            <span>تماس باما</span>
            <span>{props.match.params.code}</span>
        </h1>
    </section>
);
