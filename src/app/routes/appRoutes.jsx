import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Homepage from 'AppRoot/pages/homepage/homepage';
import NotFound from 'AppRoot/pages/notFound/notFound';
import Contact from 'AppRoot/pages/contact/contact';
import UsersList from 'AppRoot/pages/users/usersList';
import TransitionsList from 'AppRoot/pages/transitions/transitionsList';
import AccountsList from 'AppRoot/pages/accounts/accountsList';
import TicketsList from 'AppRoot/pages/tickets/ticketsList';
import Login from 'AppRoot/pages/login/login';
import styles from 'StylesRoot/styles.pcss';
import {NavLink} from 'react-router-dom';
//import AppInteraction from 'AppRoot/public/appInteraction';
import Menu from 'AppRoot/public/menu';
import Hidden from '@material-ui/core/Hidden';


export default (props) => (
    props.login
        ? (

            <div className={styles['app-wrapper']}>
                <Hidden only={['xs', 'sm', 'md']} implementation='css'>
                    <div className={styles['app-wrapper__side']}>
                        <div className={styles['app-wrapper__side__top']}>
                            <NavLink to='/'>لوگوی اسکن</NavLink>
                        </div>
                        <div className={styles['app-wrapper__side__body']}>
                            <Menu key='menu'/>
                        </div>
                    </div>
                </Hidden>
                <div className={styles['app-wrapper__main']}>
                    <div className={styles['app-wrapper__main__body']}>
                        <Switch>
                            <Route exact path='/' component={Homepage}/>
                            <Route exact path='/users-list' render={() => <UsersList fetchManager={props.fetchManager}/>}/>
                            <Route exact path='/transitions-list' render={() => <TransitionsList fetchManager={props.fetchManager}/>}/>
                            <Route exact path='/accounts-list' render={() => <AccountsList fetchManager={props.fetchManager}/>}/>
                            <Route exact path='/tickets-list' render={() => <TicketsList fetchManager={props.fetchManager}/>}/>
                            <Route exact path='/contact' component={Contact}/>
                            <Route exact path='/404' component={NotFound}/>
                            <Redirect from='/**/' to='/404'/>
                        </Switch>
                    </div>
                </div>
            </div>
        )
        : (
            <Switch>
                <Route path='/' render={() => <Login loginHandler={props.loginHandler}/>}/>
            </Switch>
        )
);
