module.exports = {
    "clientId": "client-id-support-web",
    "clientSecret": "client-secret-support-web",
    "basicAuth": "Basic Y2xpZW50LWlkLXN1cHBvcnQtd2ViOmNsaWVudC1zZWNyZXQtc3VwcG9ydC13ZWI=",
    "website": "http://37.58.51.240:9091",
    "websiteStorage": "http://37.58.51.240:9091/storage-service/api/v1/download"
};
