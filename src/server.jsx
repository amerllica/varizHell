    import React from 'react';
    import {renderToString} from 'react-dom/server';
    import {SheetsRegistry} from 'react-jss/lib/jss';
    import JssProvider from 'react-jss/lib/JssProvider';
    import {StaticRouter} from 'react-router-dom';
    import {Helmet} from "react-helmet";
    import {MuiThemeProvider, createMuiTheme, createGenerateClassName} from '@material-ui/core/styles';
    import {red, lightBlue} from '@material-ui/core/colors';
    import Template from './app/template';
    import Index from './app/index';
    import RTL from './app/public/rtl';

    export default function serverRenderer({clientStats, serverStats}) {
        return (req, res, next) => {
            const context = {};
            const sheetsRegistry = new SheetsRegistry();
            const theme = createMuiTheme({
                palette: {
                    primary: lightBlue,
                    accent: red,
                    type: 'light',
                },
                direction: 'rtl',
            });
            const generateClassName = createGenerateClassName();
            const markup = renderToString(
                <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
                    <RTL>
                        <MuiThemeProvider theme={theme} sheetsManager={new Map()}>
                            <StaticRouter location={req.url} context={context}>
                                <Index/>
                            </StaticRouter>
                        </MuiThemeProvider>
                    </RTL>
                </JssProvider>
            );
            const helmet = Helmet.renderStatic();

            const jss = sheetsRegistry.toString();

            res.status(200).send(Template({
                markup: markup,
                helmet: helmet,
                jss: jss,
            }));
        };
    }
